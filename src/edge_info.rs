use std::cell::RefCell;
use std::rc::Rc;

use crate::face_info::FaceRef;

pub type EdgeRef = Rc<RefCell<EdgeInfo>>;

/// Nice and dumb edge class that knows its
/// indices, the two faces, and the next edge using
/// the lesser of the indices.
#[derive(Debug, Clone, PartialEq)]
pub struct EdgeInfo {
    pub face0: Option<FaceRef>,
    pub face1: Option<FaceRef>,
    pub v0: u32,
    pub v1: u32,
    // todo lifetimes?
    pub next_v0: Option<EdgeRef>,
    pub next_v1: Option<EdgeRef>,
}

impl EdgeInfo {
    pub fn new(v0: u32, v1: u32) -> Self {
        Self {
            face0: None,
            face1: None,
            v0,
            v1,
            next_v0: None,
            next_v1: None,
        }
    }

    pub fn new_ref(v0: u32, v1: u32) -> EdgeRef {
        Rc::new(RefCell::new(Self::new(v0, v1)))
    }
}
