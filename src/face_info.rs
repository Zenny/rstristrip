use std::cell::RefCell;
use std::rc::Rc;

pub type FaceRef = Rc<RefCell<FaceInfo>>;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct FaceInfo {
    /// Vertex index 0.
    pub v0: u32,
    /// Vertex index 1.
    pub v1: u32,
    /// Vertex index 2.
    pub v2: u32,

    /// Real strip Id.
    pub strip_id: Option<u32>,
    /// Strip Id in an experiment.
    pub test_strip_id: Option<u32>,

    /// In what experiment was it given an experiment Id?
    pub experiment_id: Option<u32>,

    /// If true, will be deleted when the strip it's in is deleted.
    pub is_fake: bool,
}

impl FaceInfo {
    pub fn new(v0: u32, v1: u32, v2: u32, is_fake: bool) -> Self {
        Self {
            v0,
            v1,
            v2,
            strip_id: None,
            test_strip_id: None,
            experiment_id: None,
            is_fake,
        }
    }

    pub fn new_ref(v0: u32, v1: u32, v2: u32, is_fake: bool) -> FaceRef {
        Rc::new(RefCell::new(Self {
            v0,
            v1,
            v2,
            strip_id: None,
            test_strip_id: None,
            experiment_id: None,
            is_fake,
        }))
    }

    pub fn is_degenerate(&self) -> bool {
        self.v0 == self.v1 || self.v0 == self.v2 || self.v1 == self.v2
    }
}

