/// Enumeration for the types of primitives used in primitive groups.
#[derive(Debug, Copy, Clone, PartialEq, Hash)]
pub enum PrimitiveType {
    /// List of triangles consisting out of 3 indices per triangle.
    TriangleList,
    /// Triangle strip consisting out of 2 base indices, with each subsequent index forming a new triangle.
    TriangleStrip,
    /// Triangle fan consisting out of a center vertex index, with each subsequent pair of indices forming a new triangle.
    TriangleFan,
}