use std::collections::{HashSet, VecDeque};
use std::ops::{Deref, DerefMut};
use crate::edge_info::{EdgeInfo, EdgeRef};
use crate::face_info::{FaceInfo, FaceRef};
use crate::strip_info::StripInfo;
use crate::strip_start_info::StripStartInfo;

const CACHE_INEFFICIENCY: usize = 6;

/// The actual stripifier.
#[derive(Debug, Clone, PartialEq)]
pub struct Stripifier {
    pub indices: Vec<u32>,
    pub cache_size: usize,
    pub min_strip_len: usize,
    pub mesh_jump: f32,
    pub first_time_reset_point: bool,
}

impl Stripifier {
    pub fn new() -> Self {
        Self {
            indices: vec![],
            cache_size: 0,
            min_strip_len: 0,
            mesh_jump: 0.0,
            first_time_reset_point: false,
        }
    }

    pub fn stripify(&mut self, indices: Vec<u32>, cache_size: usize, min_strip_len: usize, max_index: u16, out_strips: &mut Vec<StripInfo>, out_face_list: &mut Vec<FaceInfo>) {
        self.mesh_jump = 0.0;

        // used in FindGoodResetPoint()
        self.first_time_reset_point = true;

        // the number of times to run the experiments
        let num_samples = 10;

        // the cache size, clamped to one
        self.cache_size = 1.max(cache_size - CACHE_INEFFICIENCY);

        // this is the strip size threshold below which we dump the strip into a list
        self.min_strip_len = min_strip_len;

        self.indices = indices;

        let num_faces = self.indices.len() / 3;

        // build the stripification info
        let mut all_faces = Vec::with_capacity(num_faces);
        let mut all_edges = Vec::with_capacity(self.indices.len());

        self.build_stripify_info(&mut all_faces, &mut all_edges, max_index);

        let mut all_strips = Vec::with_capacity(num_faces);

        // stripify
        self.find_all_strips(&mut all_strips, &all_faces, &all_edges, num_samples);

        // split up the strips into cache friendly pieces, optimize them, then dump these into outStrips

    }

    /// Does the stripification, puts output strips into vector allStrips.
    /// Works by runnning a number of experiments in different areas of the mesh, and
    /// accepting the one which results in the longest strips. It then accepts this, and moves
    /// on to a different area of the mesh. We try to jump around the mesh some, to ensure that
    /// large open spans of strips get generated.
    fn find_all_strips(&mut self, all_strips: &mut Vec<StripInfo>, faces: &Vec<FaceRef>, edges: &Vec<Option<EdgeRef>>, num_samples: usize) {
        let mut experiment_id = 0;
        let mut strip_id = 0;
        let mut done = false;

        let mut loop_ctr = 0;

        while !done {
            loop_ctr += 1;

            //
            // PHASE 1: Set up numSamples * numEdges experiments
            //
            let mut experiments = vec![Vec::with_capacity(128); num_samples * 6];

            let mut experiment_index = 0;
            let mut reset_points = HashSet::with_capacity(num_samples);
            for _ in 0..num_samples {
                // Try to find another good reset point.
                // If there are none to be found, we are done
                let next_face = self.find_good_reset_point(faces, edges);
                let Some(next_face) = next_face else {
                    done = true;
                    break;
                };

                let next_ref = next_face.borrow();
                // If we have already evaluated starting at this face in this slew
                // of experiments, then skip going any further
                if reset_points.contains(next_ref.deref()) {
                    continue;
                }

                // trying it now...
                reset_points.insert(next_ref.clone());

                // otherwise, we shall now try experiments for starting on the 01, 12, and 20 edges
                assert!(next_ref.strip_id.is_none());

                // build the strip off of this face's 0-1 edge
                let edge01 = find_edge_info(edges, next_ref.v0, next_ref.v1).expect("find_all_strips edge01");
                let strip01 = StripInfo::new(StripStartInfo::new(Some(next_face.clone()), Some(edge01), true), strip_id, Some(experiment_id));
                experiments[experiment_index].push(strip01);
                strip_id += 1;
                experiment_id += 1;
                experiment_index += 1;

                // build the strip off of this face's 1-0 edge
                let edge10 = find_edge_info(edges, next_ref.v0, next_ref.v1).expect("find_all_strips edge10");
                let strip10 = StripInfo::new(StripStartInfo::new(Some(next_face.clone()), Some(edge10), false), strip_id, Some(experiment_id));
                experiments[experiment_index].push(strip10);
                strip_id += 1;
                experiment_id += 1;
                experiment_index += 1;

                // build the strip off of this face's 1-2 edge
                let edge12 = find_edge_info(edges, next_ref.v1, next_ref.v2).expect("find_all_strips edge12");
                let strip12 = StripInfo::new(StripStartInfo::new(Some(next_face.clone()), Some(edge12), true), strip_id, Some(experiment_id));
                experiments[experiment_index].push(strip12);
                strip_id += 1;
                experiment_id += 1;
                experiment_index += 1;

                // build the strip off of this face's 2-1 edge
                let edge21 = find_edge_info(edges, next_ref.v1, next_ref.v2).expect("find_all_strips edge21");
                let strip21 = StripInfo::new(StripStartInfo::new(Some(next_face.clone()), Some(edge21), false), strip_id, Some(experiment_id));
                experiments[experiment_index].push(strip21);
                strip_id += 1;
                experiment_id += 1;
                experiment_index += 1;

                // build the strip off of this face's 2-0 edge
                let edge20 = find_edge_info(edges, next_ref.v2, next_ref.v0).expect("find_all_strips edge20");
                let strip20 = StripInfo::new(StripStartInfo::new(Some(next_face.clone()), Some(edge20), true), strip_id, Some(experiment_id));
                experiments[experiment_index].push(strip20);
                strip_id += 1;
                experiment_id += 1;
                experiment_index += 1;

                // build the strip off of this face's 0-2 edge
                let edge02 = find_edge_info(edges, next_ref.v2, next_ref.v0).expect("find_all_strips edge20");
                let strip02 = StripInfo::new(StripStartInfo::new(Some(next_face.clone()), Some(edge02), false), strip_id, Some(experiment_id));
                experiments[experiment_index].push(strip02);
                strip_id += 1;
                experiment_id += 1;
                experiment_index += 1;
            }

            //
            // PHASE 2: Iterate through that we setup in the last phase
            // and really build each of the strips and strips that follow to see how
            // far we get
            //
            let num_experiments = experiment_index;
            for i in 0..num_experiments {
                // get the strip set

                // build the first strip of the list
                experiments[i][0].build(edges, faces);
                experiment_id = experiments[i][0].experiment_id.expect("find_all_strips experiment_id");

                let mut strip_iter = &experiments[i][0];
                let mut start_info = StripStartInfo::new(None, None, false);
                while find_traversal(edges, strip_iter, &mut start_info) {
                    // create the new strip info
                    let mut new_strip = StripInfo::new(start_info.clone(), strip_id, Some(experiment_id));
                    strip_id += 1;

                    // build the next strip
                    new_strip.build(edges, faces);

                    let strip_ind = experiments[i].len();

                    // add it to the list
                    experiments[i].push(new_strip);

                    // borrow the new strip from the vec
                    strip_iter = &experiments[i][strip_ind];
                }
            }

            //
            // Phase 3: Find the experiment that has the most promise
            //
            let mut best_index = 0;
            let mut best_value = 0.0;
            for i in 0..num_experiments {
                let avg_strip_size_weight = 1.0;
                // let num_tris_weight = 0.0;
                let num_strips_weight = 0.0;
                let avg_strip_size = avg_strip_size(&experiments[i]);
                let num_strips = experiments[i].len() as f32;
                let value = avg_strip_size * avg_strip_size_weight + (num_strips * num_strips_weight);
                // let value = 1.0 / num_strips;
                // let value = num_strips * avg_strip_size;

                if value > best_value {
                    best_value = value;
                    best_index = i;
                }
            }

            //
            // Phase 4: commit the best experiment of the bunch
            //
            commit_strips(all_strips, experiments.remove(best_index));

            // and destroy all of the others

            // todo note: it doesn't seem like we actually need to clear this,
            //  the while loop is already going to make a new one. This step
            //  I think is meant to just be memory management in C++
            /*for experiment in experiments.iter_mut() {
                experiment.clear();
            }*/
        }
    }

    /// Splits the input vector of strips (allStrips) into smaller, cache friendly pieces, then
    /// reorders these pieces to maximize cache hits
    /// The final strips are output through outStrips
    fn split_up_strips_and_optimize(&self, all_strips: Vec<StripInfo>, edges: &Vec<Option<EdgeRef>>, out_strips: &mut Vec<StripInfo>, out_faces: &mut Vec<FaceRef>) {
        let threshold = self.cache_size;
        let mut temp_strips = Vec::with_capacity(all_strips.len());

        // split up strips into threshold-sized pieces
        for all_strip in all_strips {
            let mut cur_strip;
            // todo is this supposed to be a reference?
            let start_info = StripStartInfo::new(None, None, false);

            let mut actual_strip_size = 0;
            for face in all_strip.faces.iter() {
                let borrowed = face.borrow();
                if !is_degenerate(borrowed.v0, borrowed.v1, borrowed.v2) {
                    actual_strip_size += 1;
                }
            }

            if actual_strip_size /*allStrips[i].m_faces.Count*/ > threshold {
                let num_times = actual_strip_size /*allStrips[i].m_faces.Count*/ / threshold;
                let mut num_left_over = actual_strip_size /*allStrips[i].m_faces.Count*/ % threshold;

                let mut degenerate_count = 0;
                let mut j = 0;
                while j < num_times {
                    cur_strip = StripInfo::new(start_info.clone(), 0, None);

                    let mut face_ctr = j * threshold + degenerate_count;
                    let mut first_time = true;
                    while face_ctr < threshold + (j * threshold) + degenerate_count {
                        let this_face = &all_strip.faces[face_ctr];
                        let this_borrow = this_face.borrow();
                        if is_degenerate(this_borrow.v0, this_borrow.v1, this_borrow.v2) {
                            degenerate_count += 1;

                            // last time or first time through, no need for a degenerate\
                            if (((face_ctr + 1) != threshold + (j * threshold) + degenerate_count) ||
                                ((j == num_times - 1) && (num_left_over < 4) && (num_left_over > 0))) &&
                                !first_time {
                                cur_strip.faces.push(this_face.clone());
                                face_ctr += 1;
                            } else {
                                // but, we do need to delete the degenerate, if it's marked fake, to avoid leaking
                                // if(allStrips[i].m_faces[faceCtr].m_bIsFake)
                                // {
                                // delete allStrips[i].m_faces[faceCtr], allStrips[i].m_faces[faceCtr] = null;
                                // }
                                face_ctr += 1;
                            }
                        } else {
                            cur_strip.faces.push(this_face.clone());
                            face_ctr += 1;
                            first_time = false;
                        }
                    }

                    /*
                    for(int faceCtr = j*threshold; faceCtr < threshold+(j*threshold); faceCtr++)
                    {
                        currentStrip.m_faces.Add(allStrips[i].m_faces[faceCtr]);
                    }
                    */

                    // last time through
                    if j == num_times - 1 {
                        // way too small
                        if num_left_over< 4 && num_left_over > 0 {
                            // just add to last strip
                            let mut ctr = 0;
                            let this_face = &all_strip.faces[face_ctr];
                            let this_borrow = this_face.borrow();
                            while ctr < num_left_over {
                                if is_degenerate(this_borrow.v0, this_borrow.v1, this_borrow.v2) {
                                    degenerate_count += 1;
                                } else {
                                    ctr += 1;
                                }

                                cur_strip.faces.push(this_face.clone());
                                face_ctr += 1;
                            }

                            num_left_over = 0;
                        }
                    }

                    temp_strips.push(cur_strip);

                    j += 1;
                }

                let mut left_off = j * threshold + degenerate_count;

                if num_left_over != 0 {
                    cur_strip = StripInfo::new(start_info.clone(), 0, None);

                    let mut ctr = 0;
                    let mut first_time = true;
                    while ctr < num_left_over {
                        let this_face = &all_strip.faces[left_off];
                        let this_borrow = this_face.borrow();
                        if !is_degenerate(this_borrow.v0, this_borrow.v1, this_borrow.v2) {
                            ctr += 1;
                            first_time = false;
                            cur_strip.faces.push(this_face.clone());
                            left_off += 1;
                        } else if !first_time {
                            cur_strip.faces.push(this_face.clone());
                            left_off += 1;
                        } else {
                            // don't leak
                            // if(allStrips[i].m_faces[leftOff].m_bIsFake)
                            // {
                            // delete allStrips[i].m_faces[leftOff], allStrips[i].m_faces[leftOff] = null;
                            // }
                            left_off += 1;
                        }
                    }
                    /*
                    for(int k = 0; k < numLeftover; k++)
                    {
                        currentStrip.m_faces.Add(allStrips[i].m_faces[leftOff++]);
                    }
                    */

                    temp_strips.push(cur_strip);
                }
            } else {
                cur_strip = StripInfo::new(start_info.clone(), 0, None);

                for face in &all_strip.faces {
                    cur_strip.faces.push(face.clone());
                }

                temp_strips.push(cur_strip);
            }
        }

        // add small strips to face list
        let mut temp_strips2 = Vec::with_capacity(all_strips.len());
        self.remove_small_strips(temp_strips, &mut temp_strips2, out_faces);

        out_strips.clear();
        // screw optimization for now
        //	for(i = 0; i < tempStrips.Count; ++i)
        //    outStrips.Add(tempStrips[i]);

        if temp_strips2.len() != 0 {
            // Optimize for the vertex cache
            let mut vcache = VecDeque::with_capacity(self.cache_size);

            let mut best_num_hits;
            let mut num_hits;
            let mut best_index = 0;
            //let mut done = false

            let mut first_index = 0;
            let mut min_cost = 10000.0;

            for (i, strip) in temp_strips2.iter().enumerate() {
                let mut neighbors = 0;

                // find strip with least number of neighbors per face
                for face in &strip.faces {
                    let borrowed = face.borrow();
                    neighbors += num_neighbors(borrowed.deref(), edges);
                }

                let cur_cost = neighbors as f32 / strip.faces.len() as f32;
                if cur_cost < min_cost {
                    min_cost = cur_cost;
                    first_index = i;
                }
            }

            // Diff: We remove here instead of using the was_visited like the original
            // todo remove was_visited if this is the only place it's used
            let mut strip = temp_strips2.remove(first_index);
            update_cache_strip(&mut vcache, &strip);
            strip.was_visited = true;

            let wants_cw = strip.faces.len() % 2 == 0;

            // this n^2 algo is what slows down stripification so much....
            // needs to be improved
            loop {
                best_num_hits = -1.0;

                // find best strip to add next, given the current cache
                for (i, strip) in temp_strips2.iter().enumerate() {
                    // note: in this port all visited strips were removed
                    // if strip.was_visited {
                    //     continue;
                    // }

                    num_hits = calc_num_hits_strip(&mut vcache, strip);
                    // todo is this else-if a typo?
                    if num_hits > best_num_hits {
                        best_num_hits = num_hits;
                        best_index = i;
                    } else if num_hits >= best_num_hits {
                        // todo left off here
                    }
                }
            }
        }
    }

    /// all_strips: the whole strip vector... all small strips will be deleted from this list, to avoid leaking mem.
    /// all_big_strips: an out parameter which will contain all strips above minStripLength.
    /// face_list: an out parameter which will contain all faces which were removed from the striplist.
    fn remove_small_strips(&self, all_strips: Vec<StripInfo>, all_big_strips: &mut Vec<StripInfo>, face_list: &mut Vec<FaceRef>) {
        // make sure these are empty
        face_list.clear();
        all_big_strips.clear();
        let mut temp_face_list = Vec::with_capacity(face_list.capacity());

        for strip in all_strips {
            if strip.faces.len() < self.min_strip_len {
                // strip is too small, add faces to faceList
                temp_face_list.extend(strip.faces.iter().cloned());
            } else {
                all_big_strips.push(strip);
            }
        }

        if temp_face_list.len() > 0 {
            let mut visited_list = vec![false; temp_face_list.len()];
            let mut vcache = VecDeque::with_capacity(self.cache_size);

            let mut best_num_hits;
            let mut num_hits;
            let mut best_index = 0;

            loop {
                best_num_hits = -1;

                // find best face to add next, given the current cache
                for (i, face) in temp_face_list.iter().enumerate() {
                    if visited_list[i] {
                        continue;
                    }

                    let borrowed = face.borrow();

                    num_hits = calc_num_hits_face(&vcache, borrowed.deref()) as isize;
                    if num_hits > best_num_hits {
                        best_num_hits = num_hits;
                        best_index = i as isize;
                    }
                }

                if best_num_hits == -1 {
                    break;
                }

                let ubest_index = best_index as usize;
                visited_list[ubest_index] = true;
                let temp_face = temp_face_list[ubest_index].clone();

                let borrowed = temp_face.borrow();
                update_cache_face(&mut vcache, borrowed.deref());
                drop(borrowed);

                face_list.push(temp_face);
            }
        }
    }

    /// A good reset point is one near other commited areas so that
    /// we know that when we've made the longest strips its because
    /// we're stripifying in the same general orientation.
    fn find_good_reset_point(
        &mut self,
        faces: &Vec<FaceRef>,
        edges: &Vec<Option<EdgeRef>>,
    ) -> Option<FaceRef> {
        let mut result = None;

        let num_faces = faces.len();
        let mut start_point: isize;

        if self.first_time_reset_point {
            // first time, find a face with few neighbors (look for an edge of the mesh)
            start_point = find_start_point(faces, edges);
            self.first_time_reset_point = false;
        } else {
            start_point = ((num_faces - 1) as f32 * self.mesh_jump) as isize;
        }

        if start_point == -1 {
            start_point = ((num_faces - 1) as f32 * self.mesh_jump) as isize;

            //meshJump += 0.1f;
            //if (meshJump > 1.0f)
            //	meshJump = .05f;
        }

        // it should be positive at this point
        let start_point =  usize::try_from(start_point).expect("start_point not positive");

        let mut i = start_point;
        loop {
            // if this face isn't visited, try it
            if faces[i].borrow().strip_id.is_none() {
                result = Some(faces[i].clone());
                break;
            }

            // update the index and clamp to 0-(num_faces-1)
            i += 1;
            if i == num_faces {
                i = 0;
            }

            if i == start_point {
                break; // All faces are visited, exit the loop.
            }
        }

        // update the mesh_jump
        self.mesh_jump += 0.1;
        if self.mesh_jump > 1.0 {
            self.mesh_jump = 0.05;
        }

        result
    }


    /// Builds the list of all face and edge infos.
    fn build_stripify_info(&self, faces: &mut Vec<FaceRef>, edges: &mut Vec<Option<EdgeRef>>, max_ind: u16) {
        edges.resize(max_ind as usize + 1, None);

        let num_triangles = self.indices.len() / 3;
        let mut index = 0;
        let mut face_updated = [false; 3];

        // iterate through the triangles of the triangle list
        for _ in 0..num_triangles {
            let mut might_already_exist = true;
            face_updated = [false; 3];

            // grab the indices
            let v0 = self.indices[index];
            let v1 = self.indices[index + 1];
            let v2 = self.indices[index + 2];
            index += 3;

            // we disregard degenerates
            if is_degenerate(v0, v1, v2) {
                continue;
            }

            // create the face info and add it to the list of faces, but only if this exact face doesn't already
            // exist in the list
            let face = FaceInfo::new_ref(v0, v1, v2, false);

            // grab the edge infos, creating them if they do not already exist
            let edge01 = update_edge_face(face.clone(), v0, v1, edges, unsafe { face_updated.get_unchecked_mut(0) }, &mut might_already_exist);
            let edge12 = update_edge_face(face.clone(), v1, v2, edges, unsafe { face_updated.get_unchecked_mut(1) }, &mut might_already_exist);
            let edge20 = update_edge_face(face.clone(), v2, v0, edges, unsafe { face_updated.get_unchecked_mut(2) }, &mut might_already_exist);

            if might_already_exist {
                if already_exists(face.borrow().deref(), faces) {
                    // cleanup pointers that point to this deleted face
                    if face_updated[0] {
                        let mut e = edge01.borrow_mut();
                        e.face1 = None;
                    }
                    if face_updated[1] {
                        let mut e = edge12.borrow_mut();
                        e.face1 = None;
                    }
                    if face_updated[2] {
                        let mut e = edge20.borrow_mut();
                        e.face1 = None;
                    }
                } else {
                    faces.push(face);
                }
            } else {
                faces.push(face);
            }

        }
    }
}

#[inline]
fn update_edge_face(face: FaceRef, v0: u32, v1: u32, edges: &mut Vec<Option<EdgeRef>>, face_updated: &mut bool, might_already_exist: &mut bool) -> EdgeRef {
    if let Some(edge) = find_edge_info(edges, v0, v1) {
        let mut e = edge.borrow_mut();
        if e.face1.is_some() {
            log::warn!("BuildStripifyInfo: > 2 triangles on an edge... uncertain consequences");
        } else {
            e.face1 = Some(face);
            *face_updated = true;
        }

        drop(e);

        edge
    } else {
        //since one of it's edges isn't in the edge data structure, it can't already exist in the face structure
        *might_already_exist = false;

        // create the info
        let new_edge = EdgeInfo::new_ref(v0, v1);

        let v0_ind = v0 as usize;
        let v1_ind = v1 as usize;

        // update the linked list on both
        let mut e = new_edge.borrow_mut();
        e.next_v0 = edges[v0_ind].clone();
        e.next_v1 = edges[v1_ind].clone();

        edges[v0_ind] = Some(new_edge.clone());
        edges[v1_ind] = Some(new_edge.clone());

        // set face 0
        e.face0 = Some(face);

        drop(e);

        new_edge
    }
}

fn already_exists(maybe: &FaceInfo, faces: &Vec<FaceRef>) -> bool {
    faces.iter().any(|face| {
        let face = face.borrow();
        face.v0 == maybe.v0 && face.v1 == maybe.v1 && face.v2 == maybe.v2
    })
}

/// find the other face sharing these vertices
/// exactly like the edge info above
pub fn find_other_face(edges: &Vec<Option<EdgeRef>>, v0: u32, v1: u32, face: &FaceInfo) -> Option<FaceRef> {
    let edge = find_edge_info(edges, v0, v1);

    if edge.is_none() && v0 == v1 {
        //we've hit a degenerate
        return None;
    }

    assert!(edge.is_some());

    let edge = edge.unwrap();
    let e = edge.borrow();
    if e.face0.as_ref().is_some_and(|f| f.borrow().deref() == face) {
        e.face1.clone()
    } else {
        e.face0.clone()
    }
}

/// Returns the number of neighbors that this face has.
fn num_neighbors(face: &FaceInfo, edges: &Vec<Option<EdgeRef>>) -> usize {
    let mut num_neighbors = 0;

    if find_other_face(edges, face.v0, face.v1, face).is_some() {
        num_neighbors += 1;
    }
    if find_other_face(edges, face.v1, face.v2, face).is_some() {
        num_neighbors += 1;
    }
    if find_other_face(edges, face.v2, face.v0, face).is_some() {
        num_neighbors += 1;
    }

    num_neighbors
}

/// Find the edge info for these two indices.
pub fn find_edge_info(edges: &Vec<Option<EdgeRef>>, v0: u32, v1: u32) -> Option<EdgeRef> {
    // we can get to it through either array
    // because the edge infos have a v0 and v1
    // and there is no order except how it was
    // first created.
    // todo maybe get unchecked is safe here?
    let mut info_iter = edges.get(v0 as usize).cloned().flatten();

    while let Some(edge) = info_iter {
        let e = edge.borrow();
        if e.v0 == v0 {
            if e.v1 == v1 {
                return Some(edge.clone());
            } else {
                info_iter = e.next_v0.clone();
            }
        } else {
            assert_eq!(e.v1, v0);

            if e.v0 == v1 {
                return Some(edge.clone());
            } else {
                info_iter = e.next_v1.clone();
            }
        }
    }

    return None;
}

pub fn is_degenerate(v0: u32, v1: u32, v2: u32) -> bool {
    v0 == v1 || v0 == v2 || v1 == v2
}

/// Finds a good starting point, namely one which has only one neighbor
pub fn find_start_point(faces: &Vec<FaceRef>, edges: &Vec<Option<EdgeRef>>) -> isize {
    let mut best_ctr = -1;
    let mut best_index = -1;

    for (i, face) in faces.iter().enumerate() {
        let mut ctr = 0;

        let borrowed = face.borrow();
        let face_ref = borrowed.deref();
        if find_other_face(edges, face_ref.v0, face_ref.v1, face_ref).is_none() {
            ctr += 1;
        }
        if find_other_face(edges, face_ref.v1, face_ref.v2, face_ref).is_none() {
            ctr += 1;
        }
        if find_other_face(edges, face_ref.v2, face_ref.v0, face_ref).is_none() {
            ctr += 1;
        }

        if ctr > best_ctr {
            best_ctr = ctr;
            best_index = i as isize;
        }
    }

    if best_ctr == 0 {
        -1
    } else {
        best_index
    }
}

/// Returns vertex of the input face which is "next" in the input index list
pub fn get_next_index(indices: &Vec<u32>, face: &FaceInfo) -> Option<u32> {
    let num_indices = indices.len();
    assert!(num_indices >= 2);

    let v0 = indices[num_indices - 2];
    let v1 = indices[num_indices - 1];

    let fv0 = face.v0;
    let fv1 = face.v1;
    let fv2 = face.v2;

    if fv0 != v0 && fv0 != v1 {
        if (fv1 != v0 && fv1 != v1) || (fv2 != v0 && fv2 != v1) {
            log::warn!("GetNextIndex: Triangle doesn't have all of its vertices, duplicate triangle probably got us derailed");
        }

        return Some(fv0);
    }

    if fv1 != v0 && fv1 != v1 {
        if (fv0 != v0 && fv0 != v1) || (fv2 != v0 && fv2 != v1) {
            log::warn!("GetNextIndex: Triangle doesn't have all of its vertices, duplicate triangle probably got us derailed");
        }

        return Some(fv1);
    }

    if fv2 != v0 && fv2 != v1 {
        if (fv0 != v0 && fv0 != v1) || (fv1 != v0 && fv1 != v1) {
            log::warn!("GetNextIndex: Triangle doesn't have all of its vertices, duplicate triangle probably got us derailed");
        }

        return Some(fv2);
    }

    // shouldn't get here, but let's try and fail gracefully
    if fv0 == fv1 || fv0 == fv2 {
        Some(fv0)
    } else if fv1 == fv0 || fv1 == fv2 {
        Some(fv1)
    } else if fv2 == fv0 || fv2 == fv1 {
        Some(fv2)
    } else {
        None
    }
}

/// Finds the next face to start the next strip on.
pub fn find_traversal(edges: &Vec<Option<EdgeRef>>, strip: &StripInfo, start_info: &mut StripStartInfo) -> bool {
    let start_edge = strip.start_info.start_edge.as_ref().expect("find_traversal no start_edge");

    // if the strip was v0->v1 on the edge, then v1 will be a vertex in the next edge.
    let v = if strip.start_info.to_v1 {
        start_edge.borrow().v1
    } else {
        start_edge.borrow().v0
    };

    let mut untouched_face = None;
    let mut edge_iter = edges[v as usize].clone();
    while let Some(edge) = edge_iter.clone() {
        let borrowed = edge.borrow();

        let face0 = borrowed.face0.clone();
        let face1 = borrowed.face1.clone();

        if face0.as_ref().is_some_and(|f| {
            let face = f.borrow();
            !strip.is_in_strip(face.deref())
        }) && face1.as_ref().is_some_and(|f| {
            let face = f.borrow();
            !strip.is_marked(face.deref())
        }) {
            untouched_face = face1;
            break;
        }

        if face1.as_ref().is_some_and(|f| {
            let face = f.borrow();
            !strip.is_in_strip(face.deref())
        }) && face0.as_ref().is_some_and(|f| {
            let face = f.borrow();
            !strip.is_marked(face.deref())
        }) {
            untouched_face = face0;
            break;
        }

        // find the next edgeIter
        edge_iter = if borrowed.v0 == v {
            borrowed.next_v0.clone()
        } else {
            borrowed.next_v1.clone()
        };
    }

    start_info.start_face = untouched_face;
    start_info.start_edge = edge_iter.clone();

    if let Some(edge) = edge_iter {
        let e = edge.borrow();

        if start_info.start_face.as_ref().is_some_and(|f| {
            let face = f.borrow();
            strip.shares_edge(face.deref(), edges)
        }) {
            start_info.to_v1 = e.v0 == v; // note! used to be v1
        } else {
            start_info.to_v1 = e.v1 == v;
        }
    }

    start_info.start_face.is_some()
}

/// Finds the average strip size of the input vector of strips.
fn avg_strip_size(strips: &Vec<StripInfo>) -> f32 {
    let accum = strips.iter().fold(0, |accum, s| {
        accum + s.faces.len() - s.degenerate_count
    });

    accum as f32 / strips.len() as f32
}

/// "Commits" the input strips by setting their m_experimentId to -1 and adding to the allStrips
///  vector
fn commit_strips(all_strips: &mut Vec<StripInfo>, strips: Vec<StripInfo>) {
    // Iterate through strips
    for mut strip in strips {
        // Tell the strip that it is now real
        strip.experiment_id = None;

        // Iterate through the faces of the strip
        // Tell the faces of the strip that they belong to a real strip now
        for face in strip.faces.iter() {
            let mut borrowed = face.borrow_mut();
            strip.mark_triangle(borrowed.deref_mut());
        }

        // add to the list of real strips
        all_strips.push(strip);
    }
}

/// Returns the number of cache hits per face in the strip.
fn calc_num_hits_strip(cache: &VecDeque<u32>, strip: &StripInfo) -> f32 {
    let mut num_hits = 0.0;
    let mut num_faces = 0.0;

    for face in strip.faces {
        let borrowed = face.borrow();
        if cache.contains(&borrowed.v0) {
            num_hits += 1.0;
        }
        if cache.contains(&borrowed.v1) {
            num_hits += 1.0;
        }
        if cache.contains(&borrowed.v2) {
            num_hits += 1.0;
        }
        num_faces += 1.0;
    }

    num_hits / num_faces
}

/// Returns the number of cache hits in the face.
fn calc_num_hits_face(cache: &VecDeque<u32>, face: &FaceInfo) -> usize {
    let mut num_hits = 0;

    if cache.contains(&face.v0) {
        num_hits += 1;
    }
    if cache.contains(&face.v1) {
        num_hits += 1;
    }
    if cache.contains(&face.v2) {
        num_hits += 1;
    }

    num_hits
}

/// Updates the input vertex cache with this strip's vertices.
fn update_cache_strip(cache: &mut VecDeque<u32>, strip: &StripInfo) {
    for face in strip.faces.iter() {
        let borrowed = face.borrow();
        if !cache.contains(&borrowed.v0) {
            cache.push_front(borrowed.v0);
        }
        if !cache.contains(&borrowed.v1) {
            cache.push_front(borrowed.v1);
        }
        if !cache.contains(&borrowed.v2) {
            cache.push_front(borrowed.v2);
        }
    }
}

/// Updates the input vertex cache with this face's vertices.
fn update_cache_face(cache: &mut VecDeque<u32>, face: &FaceInfo) {
    if !cache.contains(&face.v0) {
        cache.push_front(face.v0);
    }
    if !cache.contains(&face.v1) {
        cache.push_front(face.v1);
    }
    if !cache.contains(&face.v2) {
        cache.push_front(face.v2);
    }
}