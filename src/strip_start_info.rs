use crate::edge_info::EdgeRef;
use crate::face_info::{FaceInfo, FaceRef};

/// This class is a quick summary of parameters used
/// to begin a triangle strip. Some operations may
/// want to create lists of such items, so they were
/// pulled out into a class
#[derive(Debug, Clone, PartialEq)]
pub struct StripStartInfo {
    pub start_face: Option<FaceRef>,
    pub start_edge: Option<EdgeRef>,
    pub to_v1: bool,
}

impl StripStartInfo {
    pub fn new(start_face: Option<FaceRef>, start_edge: Option<EdgeRef>, to_v1: bool) -> Self {
        Self {
            start_face,
            start_edge,
            to_v1,
        }
    }
}