use crate::primitive_type::PrimitiveType;

/// A primitive group contains optimized and/or stripped index data in a specified primitive format.
#[derive(Debug, Clone, PartialEq, Hash)]
pub struct PrimitiveGroup {
    /// Gets the primitive type of the group.
    pub kind: PrimitiveType,
    /// Gets the indices of the group.
    pub indices: Vec<u32>,
}

impl PrimitiveGroup {
    pub fn new(kind: PrimitiveType, indices: Vec<u32>) -> Self {
        Self {
            kind,
            indices,
        }
    }

    pub fn index_count(&self) -> usize {
        self.indices.len()
    }
}