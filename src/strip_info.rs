use std::ops::{Deref, DerefMut};
use crate::edge_info::{EdgeInfo, EdgeRef};
use crate::face_info::{FaceInfo, FaceRef};
use crate::strip_start_info::StripStartInfo;
use crate::stripifier::{find_edge_info, find_other_face, get_next_index};

/// This is a summary of a strip that has been built.
#[derive(Debug, Clone, PartialEq)]
pub struct StripInfo {
    pub start_info: StripStartInfo,
    pub faces: Vec<FaceRef>,
    pub strip_id: u32,
    pub experiment_id: Option<u32>,
    pub was_visited: bool,
    pub degenerate_count: usize,
}

impl StripInfo {
    /// A little information about the creation of the triangle strips.
    pub fn new(start_info: StripStartInfo, strip_id: u32, experiment_id: Option<u32>) -> Self {
        Self {
            start_info,
            faces: vec![],
            strip_id,
            experiment_id,
            was_visited: false,
            degenerate_count: 0,
        }
    }

    pub fn is_experiment(&self) -> bool {
        self.experiment_id.is_some()
    }

    // todo face info option?
    pub fn is_in_strip(&self, face: &FaceInfo) -> bool {
        if self.is_experiment() {
            face.test_strip_id.is_some_and(|id| id == self.strip_id)
        } else {
            face.strip_id.is_some_and(|id| id == self.strip_id)
        }
    }

    /// Returns true if the input face and the current strip share an edge.
    pub fn shares_edge(&self, face: &FaceInfo, edges: &Vec<Option<EdgeRef>>) -> bool {
        //check v0.v1 edge
        let cur_edge = find_edge_info(edges, face.v0, face.v1);
        let Some(cur_edge) = cur_edge else {
            return false;
        };

        let borrowed = cur_edge.borrow();

        if borrowed.face0.as_ref().is_some_and(|f| {
            let face = f.borrow();
            self.is_in_strip(face.deref())
        }) || borrowed.face1.as_ref().is_some_and(|f| {
            let face = f.borrow();
            self.is_in_strip(face.deref())
        }) {
            return true;
        }

        //check v1.v2 edge
        let cur_edge = find_edge_info(edges, face.v1, face.v2);
        let Some(cur_edge) = cur_edge else {
            return false;
        };

        let borrowed = cur_edge.borrow();

        if borrowed.face0.as_ref().is_some_and(|f| {
            let face = f.borrow();
            self.is_in_strip(face.deref())
        }) || borrowed.face1.as_ref().is_some_and(|f| {
            let face = f.borrow();
            self.is_in_strip(face.deref())
        }) {
            return true;
        }

        //check v2.v0 edge
        let cur_edge = find_edge_info(edges, face.v2, face.v0);
        let Some(cur_edge) = cur_edge else {
            return false;
        };

        let borrowed = cur_edge.borrow();

        if borrowed.face0.as_ref().is_some_and(|f| {
            let face = f.borrow();
            self.is_in_strip(face.deref())
        }) || borrowed.face1.as_ref().is_some_and(|f| {
            let face = f.borrow();
            self.is_in_strip(face.deref())
        }) {
            return true;
        }

        false
    }

    /// Combines the two input face vectors and puts the result into m_faces.
    pub fn combine(&mut self, forward: &Vec<FaceRef>, backward: &Vec<FaceRef>) {
        // add backward faces
        for face in backward.iter().rev() {
            self.faces.push(face.clone());
        }

        // add forward faces
        for face in forward.iter() {
            self.faces.push(face.clone());
        }
    }

    /// If either the faceInfo has a real strip index because it is
    /// already assign to a committed strip OR it is assigned in an
    /// experiment and the experiment index is the one we are building
    /// for, then it is marked and unavailable
    pub fn is_marked(&self, face: &FaceInfo) -> bool {
        face.strip_id.is_some() ||  (self.is_experiment() && face.experiment_id == self.experiment_id)
    }

    ///  Marks the face with the current strip ID.
    pub fn mark_triangle(&self, face: &mut FaceInfo) {
        assert!(!self.is_marked(face));
        if self.is_experiment() {
            face.experiment_id = self.experiment_id;
            face.test_strip_id = Some(self.strip_id);
        } else {
            assert!(face.strip_id.is_none());
            face.experiment_id = None;
            face.strip_id = Some(self.strip_id);
        }
    }

    /// Builds a strip forward as far as we can go, then builds backwards, and joins the two lists.
    pub fn build(&mut self, edges: &Vec<Option<EdgeRef>>, faces: &Vec<FaceRef>) {
        // used in building the strips forward and backward
        let mut scratch_indices = Vec::with_capacity(256);

        // build forward... start with the initial face
        let mut forward_faces = Vec::with_capacity(faces.len());
        let mut backward_faces = Vec::with_capacity(faces.len());

        let start_face = self.start_info.start_face.as_ref().expect("build no start_face");
        let start_edge = self.start_info.start_edge.as_ref().expect("build no start_edge");

        forward_faces.push(start_face.clone());

        let mut start_borrowed = start_face.borrow_mut();

        self.mark_triangle(start_borrowed.deref_mut());

        let v0 = if self.start_info.to_v1 {
            start_edge.borrow().v0
        } else {
            start_edge.borrow().v1
        };

        let v1 = if self.start_info.to_v1 {
            start_edge.borrow().v1
        } else {
            start_edge.borrow().v0
        };

        // easiest way to get v2 is to use this function which requires the
        // other indices to already be in the list.
        scratch_indices.push(v0);
        scratch_indices.push(v1);
        let v2 = get_next_index(&scratch_indices, start_borrowed.deref()).expect("build no v2");
        scratch_indices.push(v2);

        //
        // build the forward list
        //
        let mut nv0 = v1;
        let mut nv1 = v2;

        let mut next_face = find_other_face(edges, nv0, nv1, start_borrowed.deref());

        while let Some(face) = next_face {
            let mut borrowed = face.borrow_mut();
            let face_ref = borrowed.deref();
            if self.is_marked(face_ref) {
                break;
            }

            // check to see if this next face is going to cause us to die soon
            let mut test_nv0 = nv1;
            let test_nv1 = get_next_index(&scratch_indices, face_ref).expect("build 1 test_nv1");

            let next_next_face = find_other_face(edges, test_nv0, test_nv1, face_ref);

            if next_next_face.is_none() || next_next_face.is_some_and(|f| {
                let borrowed = f.borrow();
                self.is_marked(borrowed.deref())
            }) {
                // uh, oh, we're following a dead end, try swapping
                let test_next_face = find_other_face(edges, nv0, test_nv1, face_ref);

                if let Some(test_next) = test_next_face {
                    let test_next_borrowed = test_next.borrow();
                    if !self.is_marked(test_next_borrowed.deref()) {
                        // we only swap if it buys us something

                        // add a "fake" degenerate face
                        let temp_face = FaceInfo::new_ref(nv0, nv1, nv0, true);
                        let mut temp_borrowed = temp_face.borrow_mut();

                        forward_faces.push(temp_face.clone());
                        self.mark_triangle(temp_borrowed.deref_mut());

                        scratch_indices.push(nv0);
                        test_nv0 = nv0;

                        self.degenerate_count += 1;
                    }
                }
            }

            // add this to the strip
            forward_faces.push(face.clone());
            self.mark_triangle(borrowed.deref_mut());

            // add the index
            //nv0 = nv1;
            //nv1 = Stripifier.GetNextIndex(scratchIndices, nextFace);
            scratch_indices.push(test_nv1);

            // and get the next face
            nv0 = test_nv0;
            nv1 = test_nv1;

            next_face = find_other_face(edges, nv0, nv1, borrowed.deref());
        }

        // tempAllFaces is going to be forwardFaces + backwardFaces
        // it's used for Unique()
        let mut temp_all_faces = forward_faces.clone();

        //
        // reset the indices for building the strip backwards and do so
        //
        scratch_indices.clear();
        scratch_indices.push(v2);
        scratch_indices.push(v1);
        scratch_indices.push(v0);

        nv0 = v1;
        nv1 = v0;

        next_face = find_other_face(edges, nv0, nv1, start_borrowed.deref());
        while let Some(face) = next_face {
            let mut borrowed = face.borrow_mut();
            let face_ref = borrowed.deref();
            // this tests to see if a face is "unique", meaning that its vertices aren't already in the list
            // so, strips which "wrap-around" are not allowed
            if self.is_marked(face_ref) || !unique(&temp_all_faces, face_ref) {
                break;
            }

            // check to see if this next face is going to cause us to die soon
            let mut test_nv0 = nv1;
            let test_nv1 = get_next_index(&scratch_indices, face_ref).expect("build 2 test_nv1");

            let next_next_face = find_other_face(edges, test_nv0, test_nv1, face_ref);

            if next_next_face.is_none() || next_next_face.is_some_and(|f| {
                let borrowed = f.borrow();
                self.is_marked(borrowed.deref())
            }) {
                // uh, oh, we're following a dead end, try swapping
                let test_next_face = find_other_face(edges, nv0, test_nv1, face_ref);

                if let Some(test_next) = test_next_face {
                    let test_next_borrowed = test_next.borrow();
                    if !self.is_marked(test_next_borrowed.deref()) {
                        // we only swap if it buys us something

                        // add a "fake" degenerate face
                        let temp_face = FaceInfo::new_ref(nv0, nv1, nv0, true);
                        let mut temp_borrowed = temp_face.borrow_mut();

                        backward_faces.push(temp_face.clone());
                        self.mark_triangle(temp_borrowed.deref_mut());

                        scratch_indices.push(nv0);
                        test_nv0 = nv0;

                        self.degenerate_count += 1;
                    }
                }
            }

            // add this to the strip
            backward_faces.push(face.clone());

            //this is just so Unique() will work
            temp_all_faces.push(face.clone());

            self.mark_triangle(borrowed.deref_mut());

            // add the index
            //nv0 = nv1;
            //nv1 = Stripifier.GetNextIndex(scratchIndices, nextFace);
            scratch_indices.push(test_nv1);

            nv0 = test_nv0;
            nv1 = test_nv1;

            next_face = find_other_face(edges, nv0, nv1, borrowed.deref());
        }

        drop(start_borrowed);

        self.combine(&forward_faces, &backward_faces);
    }
}

/// returns true if the face is "unique", i.e. has a vertex which doesn't exist in the faceVec.
pub fn unique(faces: &Vec<FaceRef>, face: &FaceInfo) -> bool {
    let mut bv = [false; 3];

    for cur_face in faces {
        let cur = cur_face.borrow();
        if !bv[0] {
            if cur.v0 == face.v0 || cur.v1 == face.v0 || cur.v2 == face.v0 {
                bv[0] = true;
            }
        }
        if !bv[1] {
            if cur.v0 == face.v1 || cur.v1 == face.v1 || cur.v2 == face.v1 {
                bv[1] = true;
            }
        }
        if !bv[2] {
            if cur.v0 == face.v2 || cur.v1 == face.v2 || cur.v2 == face.v2 {
                bv[2] = true;
            }
        }

        if bv[0] && bv[1] && bv[2] {
            return false;
        }
    }

    true
}